# RestCalculator
Repository link:
https://gitlab.com/pietrucha1991/restcalculator

This project was a task for one of the companies I applied to as a Junior Java Developer.
This calculator is meant to do simple calculations on three unit types - Feet, Nautical miles and meters. 
It should be able to convert units to the target values. It should be built using springboot and java 8/11.
