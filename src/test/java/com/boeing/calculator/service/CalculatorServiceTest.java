package com.boeing.calculator.service;

import com.boeing.calculator.dto.InputDto;
import com.boeing.calculator.dto.OutputDto;
import com.boeing.calculator.exception.WrongValueException;
import com.boeing.calculator.model.ValueModel;
import com.boeing.calculator.model.ValueType;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class CalculatorServiceTest {

    @Autowired
    CalculatorService calculatorService;

    @Test
    void shouldAddWhenTwoValuesAreTheSameTypeAndOutputIsTheSame() {
        //given
        ValueModel valueModel2 = new ValueModel(12, ValueType.FEET);
        ValueModel valueModel = new ValueModel(12, ValueType.FEET);
        InputDto inputDto = new InputDto(valueModel, valueModel2, ValueType.FEET);

        //when
        OutputDto outputDto = calculatorService.add(inputDto);

        //then
        assertThat(outputDto.getNumber()).isEqualTo(24);
    }

    @Test
    void shouldAddFeetAndMetersTypeWhereOutputIsFeet() {
        //given
        ValueModel valueModelFirst = new ValueModel(12, ValueType.FEET);
        ValueModel valueModelSecond = new ValueModel(12, ValueType.METERS);
        InputDto inputDto = new InputDto(valueModelFirst, valueModelSecond, ValueType.FEET);

        //when
        OutputDto outputDto = calculatorService.add(inputDto);

        //then
        assertThat(outputDto.getNumber()).isEqualTo(51.37);
    }

    @Test
    void shouldAddFeetAndNauticalMilesTypeWhereOutputIsMeters() {
        //given
        ValueModel valueModelFirst = new ValueModel(12, ValueType.FEET);
        ValueModel valueModelSecond = new ValueModel(12, ValueType.NAUTICAL_MILES);
        InputDto inputDto = new InputDto(valueModelFirst, valueModelSecond, ValueType.METERS);

        //when
        OutputDto outputDto = calculatorService.add(inputDto);

        //then
        assertThat(outputDto.getNumber()).isEqualTo(22227.66);
    }

    @Test
    void shouldSubtractTheSameValueModelsWhereOutputIsTheSame() {
        //given
        ValueModel valueModelFirst = new ValueModel(12, ValueType.FEET);
        ValueModel valueModelSecond = new ValueModel(12, ValueType.FEET);
        InputDto inputDto = new InputDto(valueModelFirst, valueModelSecond, ValueType.FEET);

        //when
        OutputDto outputDto = calculatorService.subtract(inputDto);

        //then
        assertThat(outputDto.getNumber()).isEqualTo(0);
    }

    @Test
    void shouldSubtractDifferentValueModelsWhereOutputIsDifferent() {
        //given
        ValueModel valueModelFirst = new ValueModel(12, ValueType.NAUTICAL_MILES);
        ValueModel valueModelSecond = new ValueModel(12, ValueType.FEET);
        InputDto inputDto = new InputDto(valueModelFirst, valueModelSecond, ValueType.METERS);

        //when
        OutputDto outputDto = calculatorService.subtract(inputDto);

        //then
        assertThat(outputDto.getNumber()).isEqualTo(22220.34);
    }

    @Test
    void shouldDivideTwoTheSameValueModelsWhereOutputIsTheSame() {
        //given
        ValueModel valueModelFirst = new ValueModel(12, ValueType.FEET);
        ValueModel valueModelSecond = new ValueModel(12, ValueType.FEET);
        InputDto inputDto = new InputDto(valueModelFirst, valueModelSecond, ValueType.FEET);

        //when
        OutputDto outputDto = calculatorService.divide(inputDto);

        //then
        assertThat(outputDto.getNumber()).isEqualTo(1);
    }

    @Test
    void shouldDivideDifferentValueModelsWhereOutputIsDifferent() {
        //given
        ValueModel valueModelFirst = new ValueModel(12, ValueType.NAUTICAL_MILES);
        ValueModel valueModelSecond = new ValueModel(10, ValueType.FEET);
        InputDto inputDto = new InputDto(valueModelFirst, valueModelSecond, ValueType.METERS);

        //when
        OutputDto outputDto = calculatorService.divide(inputDto);

        //then
        assertThat(outputDto.getNumber()).isEqualTo(7291.34);
    }

    @Test
    void shouldThrowExceptionWhenDividingByZero() {
        //given
        ValueModel valueModelFirst = new ValueModel(12, ValueType.NAUTICAL_MILES);
        ValueModel valueModelSecond = new ValueModel(0, ValueType.FEET);
        InputDto inputDto = new InputDto(valueModelFirst, valueModelSecond, ValueType.METERS);

        //when, then
        assertThatThrownBy(() -> calculatorService.divide(inputDto)).isInstanceOf(WrongValueException.class);
    }

    @Test
    void shouldMultiplyTwoTheSameValueModelsWhereOutputIsTheSame() {
        //given
        ValueModel valueModelFirst = new ValueModel(12, ValueType.FEET);
        ValueModel valueModelSecond = new ValueModel(12, ValueType.FEET);
        InputDto inputDto = new InputDto(valueModelFirst, valueModelSecond, ValueType.FEET);

        //when
        OutputDto outputDto = calculatorService.multiply(inputDto);

        //then
        assertThat(outputDto.getNumber()).isEqualTo(144);
        assertThat(outputDto.getValueType()).isEqualTo(ValueType.SQUARE_FEET);
    }

    @Test
    void shouldMultiplyTwoDifferentValueModelsWhereOutputIsDifferent() {
        //given
        ValueModel valueModelFirst = new ValueModel(10, ValueType.NAUTICAL_MILES);
        ValueModel valueModelSecond = new ValueModel(10, ValueType.METERS);
        InputDto inputDto = new InputDto(valueModelFirst, valueModelSecond, ValueType.FEET);

        //when
        OutputDto outputDto = calculatorService.multiply(inputDto);

        //then
        assertThat(outputDto.getNumber()).isEqualTo(1993476.21);
        assertThat(outputDto.getValueType()).isEqualTo(ValueType.SQUARE_FEET);
    }
}
