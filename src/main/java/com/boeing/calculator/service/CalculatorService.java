package com.boeing.calculator.service;

import com.boeing.calculator.dto.InputDto;
import com.boeing.calculator.dto.OutputDto;
import com.boeing.calculator.exception.WrongValueException;
import com.boeing.calculator.model.ValueType;
import org.springframework.stereotype.Service;

import static com.boeing.calculator.model.ValueType.*;

@Service
public class CalculatorService {

    private final ValueConverter valueConverter;

    public CalculatorService(ValueConverter valueConverter) {
        this.valueConverter = valueConverter;
    }

    public OutputDto add(InputDto inputDto) {
        double firstNumber = valueConverter.convertValue(inputDto.getFirstInput(), inputDto.getOutputType());
        double secondNumber = valueConverter.convertValue(inputDto.getSecondInput(), inputDto.getOutputType());

        double result = firstNumber + secondNumber;
        double roundedResult = roundToTwoDecimals(result);

        return new OutputDto(roundedResult, inputDto.getOutputType());
    }

    public OutputDto subtract(InputDto inputDto) {
        double firstNumber = valueConverter.convertValue(inputDto.getFirstInput(), inputDto.getOutputType());
        double secondNumber = valueConverter.convertValue(inputDto.getSecondInput(), inputDto.getOutputType());

        double result = firstNumber - secondNumber;
        double roundedResult = roundToTwoDecimals(result);

        return new OutputDto(roundedResult, inputDto.getOutputType());
    }

    public OutputDto divide(InputDto inputDto) {
        double firstNumber = valueConverter.convertValue(inputDto.getFirstInput(), inputDto.getOutputType());
        double secondNumber = valueConverter.convertValue(inputDto.getSecondInput(), inputDto.getOutputType());

        if (secondNumber == 0) {
            throw new WrongValueException("Second number shouldn't be number zero");
        }

        double result = firstNumber / secondNumber;
        double roundedResult = roundToTwoDecimals(result);

        return new OutputDto(roundedResult, inputDto.getOutputType());
    }

    public OutputDto multiply(InputDto inputDto) {
        double firstNumber = valueConverter.convertValue(inputDto.getFirstInput(), inputDto.getOutputType());
        double secondNumber = valueConverter.convertValue(inputDto.getSecondInput(), inputDto.getOutputType());

        double result = firstNumber * secondNumber;
        double roundedResult = roundToTwoDecimals(result);

        ValueType targetValue = null;

        switch (inputDto.getOutputType()) {
            case FEET:
                targetValue = SQUARE_FEET;
                break;
            case METERS:
                targetValue = SQUARE_METERS;
                break;
            case NAUTICAL_MILES:
                targetValue = SQUARE_NAUTICAL_MILES;
                break;
        }

        return new OutputDto(roundedResult, targetValue);
    }

    private double roundToTwoDecimals(double sumResult) {
        return (double) Math.round(sumResult * 100) / 100;
    }
}
