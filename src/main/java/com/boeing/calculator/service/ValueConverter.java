package com.boeing.calculator.service;

import com.boeing.calculator.exception.WrongValueException;
import com.boeing.calculator.model.ValueModel;
import com.boeing.calculator.model.ValueType;
import org.springframework.stereotype.Component;

@Component
public class ValueConverter {
    private static final double metersToFeet = 3.2808399;
    private static final double metersToNauticalMiles = 0.000539956803;
    private static final double feetToMeters = 0.3048;
    private static final double feetToNauticalMiles = 0.000164578834;
    private static final double nauticalMilesToMeters = 1852;
    private static final double nauticalMilesToFeet = 6076.11549;

    public double convertValue(ValueModel valueModel, ValueType target) {
        if (isInputTheSameAsTarget(valueModel, target)) {
            return valueModel.getNumber();
        }

        double valueTypeNumber = valueModel.getNumber();
        ValueType valueTypeModel = valueModel.getInputType();

        if (valueTypeModel.equals(ValueType.FEET) && target.equals(ValueType.METERS)) {
            return valueTypeNumber * feetToMeters;
        } else if (valueTypeModel.equals(ValueType.FEET) && target.equals(ValueType.NAUTICAL_MILES)) {
            return valueTypeNumber * feetToNauticalMiles;
        } else if (valueTypeModel.equals(ValueType.METERS) && target.equals(ValueType.FEET)) {
            return valueTypeNumber * metersToFeet;
        } else if (valueTypeModel.equals(ValueType.METERS) && target.equals(ValueType.NAUTICAL_MILES)) {
            return valueTypeNumber * metersToNauticalMiles;
        } else if (valueTypeModel.equals(ValueType.NAUTICAL_MILES) && target.equals(ValueType.FEET)) {
            return valueTypeNumber * nauticalMilesToFeet;
        } else if (valueTypeModel.equals(ValueType.NAUTICAL_MILES) && target.equals(ValueType.METERS)) {
            return valueTypeNumber * nauticalMilesToMeters;
        } else {
            throw new WrongValueException("Wrong value and cannot be handled");
        }
    }

    private boolean isInputTheSameAsTarget(ValueModel valueModel, ValueType target) {
        return valueModel.getInputType().equals(target);
    }
}

