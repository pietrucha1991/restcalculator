package com.boeing.calculator.exception;

public class WrongValueException extends RuntimeException {
    public WrongValueException(String message) {
        super(message);
    }
}
