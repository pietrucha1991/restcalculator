package com.boeing.calculator.controller;

import com.boeing.calculator.dto.InputDto;
import com.boeing.calculator.dto.OutputDto;
import com.boeing.calculator.service.CalculatorService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculator")
public class CalculatorController {

    private final CalculatorService calculatorService;

    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @PostMapping("/add")
    public OutputDto add(@RequestBody InputDto inputDto) {
        return calculatorService.add(inputDto);
    }

    @PostMapping("/subtract")
    public OutputDto subtract(@RequestBody InputDto inputDto) {
        return calculatorService.subtract(inputDto);
    }

    @PostMapping("/divide")
    public OutputDto divide(@RequestBody InputDto inputDto) {
        return calculatorService.divide(inputDto);
    }

    @PostMapping("/multiply")
    public OutputDto multiply(@RequestBody InputDto inputDto) {
        return calculatorService.multiply(inputDto);
    }


}
