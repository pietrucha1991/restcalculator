package com.boeing.calculator.dto;

import com.boeing.calculator.model.ValueModel;
import com.boeing.calculator.model.ValueType;

public class InputDto {
    private ValueModel firstInput;
    private ValueModel secondInput;
    private ValueType outputType;

    public InputDto(ValueModel firstInput, ValueModel secondInput, ValueType outputType) {
        this.firstInput = firstInput;
        this.secondInput = secondInput;
        this.outputType = outputType;
    }

    public ValueModel getFirstInput() {
        return firstInput;
    }

    public void setFirstInput(ValueModel firstInput) {
        this.firstInput = firstInput;
    }

    public ValueModel getSecondInput() {
        return secondInput;
    }

    public void setSecondInput(ValueModel secondInput) {
        this.secondInput = secondInput;
    }

    public ValueType getOutputType() {
        return outputType;
    }

    public void setOutputType(ValueType outputType) {
        this.outputType = outputType;
    }
}
