package com.boeing.calculator.dto;

import com.boeing.calculator.model.ValueType;

public class OutputDto {

    private double number;
    private ValueType valueType;

    public OutputDto(double number, ValueType valueType) {
        this.number = number;
        this.valueType = valueType;
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    public ValueType getValueType() {
        return valueType;
    }

    public void setValueType(ValueType valueType) {
        this.valueType = valueType;
    }
}
