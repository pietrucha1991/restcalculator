package com.boeing.calculator.model;

public enum ValueType {
    FEET, METERS, NAUTICAL_MILES, SQUARE_METERS, SQUARE_FEET, SQUARE_NAUTICAL_MILES;

}
