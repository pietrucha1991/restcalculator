package com.boeing.calculator.model;

public class ValueModel {
    private double number;
    private ValueType inputType;

    public ValueModel(double number, ValueType inputType) {
        this.number = number;
        this.inputType = inputType;
    }

    public ValueModel() {
    }

    public double getNumber() {
        return number;
    }

    public void setNumber(double number) {
        this.number = number;
    }

    public ValueType getInputType() {
        return inputType;
    }

    public void setInputType(ValueType inputType) {
        this.inputType = inputType;
    }
}
